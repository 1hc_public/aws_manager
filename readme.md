<!-- hc_scraper documentation master file, created by
sphinx-quickstart on Thu Feb 18 01:12:35 2021.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive. -->
# Welcome to aws_manager’s documentation!

# AWS_Base


### class aws_manager.core.base.AWS_Base(aws_access_key=None, aws_secret=None)
Core credential setup.

# EC2


### class aws_manager.ec2.ec2_manager.EC2(region='us-east-1', security_groups=['master'], pem_key=None, pem_path=None, pwd='debian1234', \*\*kwargs)
High level object to simplify ec2 ops (particularly distributed).


#### boot_instances(img, instance_type='t3.nano', instance_count=1, \*\*kwargs)
Launch instance_count number of img AMI running on instance_type.


* **Parameters**

    
    * **img** (*Str*) – String corresponding to an accessible AMI.
    Ex. ‘ami-123hkj1l23h8563’


    * **instance_type** (*Str*) – Type of instance. The default is ‘t3.nano’.


    * **instance_count** (*Int*) – Number of instances to launch. The default is 1.


    * **\*\*kwargs** (*Multiple*) – Any additional kwargs to be supplied to ec2.run_instances().



* **Returns**

    List of boto object(s) representing AWS instance(s).



* **Return type**

    [boto.ec2.instance.Instance]



#### init_pclient()
Initialize parallel ssh client.


* **Returns**

    


* **Return type**

    None.



#### permission_security()
Attempt to fetch ip and permission to security group.


* **Returns**

    


* **Return type**

    None.



#### terminate_instances()
Terminate all instances associated with EC2 object.


* **Returns**

    


* **Return type**

    None.



#### update_status()
Check status of instances.


* **Returns**

    **status** – What state the instance(s) are currently in.
    For example: {i-…: ‘running’}.



* **Return type**

    {instance: str}


# S3


### class aws_manager.s3.s3_manager.S3(\*\*kwargs)
High level object to simplify s3 management.


#### build_bucket(bucket_name, bucket_kwargs={})
Create specified S3 bucket.


#### delete_bucket(bucket_name, bucket_kwargs={})
Delete specified S3 bucket.


#### delete_file(path_file)
Delete file.


#### exists(path_file)
Check if file exists.


#### last_mod(path_file)
Check and return time of last modification.


#### read_pandas(loc)
Read stream of pandas df to s3.


#### set_bucket(bucket_name)
Set bucket for S3 object.


#### write_pandas(df, loc)
Write stream of pandas df to s3.

# Image


### class aws_manager.img.image_manager.Image(region='us-east-1', security_groups=['master'], pem_key=None, pem_path=None, pwd='debian1234', \*\*kwargs)
Manage image to distribute scrape.


#### append_install_script(bash_commands=[])
Append default install script with list of commands.


#### build_image(img='ami-03d315ad33b9d49c4', instance_type='t3.nano', ec2_kwargs={})
Boot base image.


#### delete_image(img_id=None)
Deregister image.


#### register_image(image_name='hc_default')
Create image.
