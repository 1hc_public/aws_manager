.. hc_scraper documentation master file, created by
   sphinx-quickstart on Thu Feb 18 01:12:35 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to aws_manager's documentation!
=======================================

.. toctree::
   :maxdepth: 4

AWS_Base
========

.. autoclass:: aws_manager.core.base.AWS_Base
   :members:


EC2
===

.. autoclass:: aws_manager.ec2.ec2_manager.EC2
   :members:

S3
==

.. autoclass:: aws_manager.s3.s3_manager.S3
   :members:

Image
=====

.. autoclass:: aws_manager.img.image_manager.Image
   :members:
