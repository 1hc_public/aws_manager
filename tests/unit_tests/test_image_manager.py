"""Test Image functionality."""
import unittest

from aws_manager.img.image_manager import Image


img = Image()


class TestAppendScript(unittest.TestCase):
    """Test amendment of aws information script."""

    def test_append_install_script(self):
        """Test whether script appends properly."""
        old_info = img.information
        img.append_install_script(['pwd'])
        self.assertEqual(
            old_info
            + '\n' + '\n'.join(['pwd']),
            img.information
            )


class TestBuildImage(unittest.TestCase):
    """Test if image builds properly."""

    def test_build_image(self):
        """Test if image built."""
        img.build_image()
        self.assertIsNotNone(
            img.image_instance
            )


class TestRegisterImage(unittest.TestCase):
    """Test if image registers."""

    def test_register_image(self):
        """Test if image built."""
        img.register_image('hc_default')
        self.assertIsNotNone(
            img.img_id
            )


class TestDeleteImage(unittest.TestCase):
    """Test if image deregisters properly."""

    def test_delete_image(self):
        """Test if image deletes successfully."""
        self.assertTrue(
            img.delete_image()
            )
