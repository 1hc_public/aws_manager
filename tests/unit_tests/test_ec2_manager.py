"""Test EC2 functionality."""
import unittest
import time

from aws_manager.ec2.ec2_manager import EC2


ec2 = EC2()


class TestBootInstances(unittest.TestCase):
    """Test EC2 booting."""

    def test_boot_instances(self):
        """Test whether instance boots properly."""
        r = ec2.boot_instances('ami-03d315ad33b9d49c4')
        self.assertTrue(
            'Instance' in str(r)
            )


class TestPermissionSecurity(unittest.TestCase):
    """Test EC2 security group permission fx."""

    def test_permission_security(self):
        """Test whether function runs properly."""
        r = ec2.permission_security()
        self.assertIsNone(
            r
            )


class TestUpdateStatus(unittest.TestCase):
    """Test EC2 instance status updating."""

    def test_update_status(self):
        """Test whether status is returned."""
        time.sleep(10)
        r = ec2.update_status()
        self.assertIsInstance(r, dict)


class TestParallelClientInit(unittest.TestCase):
    """Test pssh init functionality."""

    def test_init_pclient(self):
        """Test to see if initiliazes w/o error."""
        time.sleep(90)
        ec2.init_pclient()
        self.assertIsNotNone(ec2.pclient)


class TestParallelClientCmd(unittest.TestCase):
    """Test pssh cmd functionality."""

    def test_simple_command(self):
        """Test run a simple command."""
        cmd = 'pwd'
        tgt = ['/home/ubuntu']
        r = ec2.pclient.run_command(cmd)
        stdout = [l for l in r[0].stdout]
        self.assertEqual(stdout, tgt)


class TestTerminateInstances(unittest.TestCase):
    """Test EC2 termination."""

    def test_terminate_instances(self):
        """Test whether instance terminates properly."""
        r = ec2.terminate_instances()
        self.assertEqual(
            r,
            None
            )
