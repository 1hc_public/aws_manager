"""Test S3 functionality."""
import unittest
import datetime

import pandas as pd

from aws_manager.s3.s3_manager import S3


s3 = S3()
df = pd.DataFrame([[*range(3)] for i in range(3)])


class TestBuildBucket(unittest.TestCase):
    """Test S3 bucket creation."""

    def test_build_bucket(self):
        """Test whether client returns correct status code."""
        r = s3.build_bucket('hc-temp-test')
        s = r['ResponseMetadata']['HTTPStatusCode']
        self.assertEqual(
            str(s)[0],
            '2'
            )


class TestWritePandas(unittest.TestCase):
    """Test writing through S3."""

    def test_write_pandas(self):
        """Test whether client returns correct status code."""
        r = s3.write_pandas(df, 'df_main.csv')
        s = r['ResponseMetadata']['HTTPStatusCode']
        self.assertEqual(
            str(s)[0],
            '2'
            )


class TestReadPandas(unittest.TestCase):
    """Test reading through S3."""

    def test_read_pandas(self):
        """Test whether expected df."""
        _df = s3.read_pandas('df_main.csv')
        self.assertEqual(
            _df.shape,
            df.shape
            )


class TestExists(unittest.TestCase):
    """Test file existence through S3."""

    def test_exists(self):
        """Test whether returns expected boolean."""
        r = s3.exists('df_main.csv')
        self.assertIsInstance(
            r,
            bool
            )


class TestLastMod(unittest.TestCase):
    """Test get last mod time through S3."""

    def test_last_mod(self):
        """Test whether returns expected dtype."""
        r = s3.last_mod('df_main.csv')
        self.assertIsInstance(
            r,
            datetime.datetime
            )


class TestDeleteFile(unittest.TestCase):
    """Test S3 file deletion."""

    def test_delete_file(self):
        """Test whether client returns correct status code."""
        r = s3.delete_file('df_main.csv')
        s = r['ResponseMetadata']['HTTPStatusCode']
        self.assertEqual(
            str(s)[0],
            '2'
            )


class TestDeleteBucket(unittest.TestCase):
    """Test S3 bucket deletion."""

    def test_delete_bucket(self):
        """Test whether client returns correct status code."""
        r = s3.delete_bucket('hc-temp-test')
        s = r['ResponseMetadata']['HTTPStatusCode']
        self.assertEqual(
            str(s)[0],
            '2'
            )
