"""Base module to make accessible an object to simplify AWS."""
# Copyright (c) 2019-2021 1 Howard Capital LLC
import os

import boto.connection as boto_connection


class AWS_Base:
    """Core credential setup."""

    def __init__(self, aws_access_key=None, aws_secret=None):
        """
        Look everywhere logical for AWS credentials if not specified.

        Parameters
        ----------
        aws_access_key : Str, optional
            AWS Access Key. The default is None.
        aws_secret : Str, optional
            AWS Secret Key. The default is None.

        Returns
        -------
        None.

        """
        self.aws_access_key = aws_access_key
        self.aws_secret = aws_secret

        if not aws_access_key and not aws_secret:
            self.aws_access_key = os.getenv('AWS_ACCESS_KEY_ID')
            self.aws_secret = os.getenv('AWS_SECRET_ACCESS_KEY')

        if not aws_access_key and not aws_secret:
            self.aws_access_key = boto_connection.AWSQueryConnection() \
                .access_key
            self.aws_secret = boto_connection.AWSQueryConnection() \
                .aws_secret_access_key
