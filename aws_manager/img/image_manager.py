"""Module to manage image."""
import time

from aws_manager.ec2.ec2_manager import EC2
from aws_manager.s3.s3_manager import S3


class Image(EC2, S3):
    """Manage image to distribute scrape."""

    def append_install_script(self, bash_commands=[]):
        """Append default install script with list of commands."""
        if bash_commands:
            self.information = self.information + \
                '\n' + '\n'.join(bash_commands)

    def build_image(self,
                    img='ami-03d315ad33b9d49c4',
                    instance_type='t3.micro',
                    ec2_kwargs={}):
        """Boot base image. Defaults to ubuntu 20 on t3.nano."""
        self.image_instance = self.boot_instances(
            img,
            instance_type=instance_type,
            instance_count=1,
            **ec2_kwargs
            )[0]

        # Command needs time to register with AWS.
        time.sleep(3)

        while 'running' != self.update_status()[self.image_instance]:
            time.sleep(10)

        # Note: Leave some time for information.txt to finish running.

    def register_image(self, image_name):
        """Create image."""
        self.img_id = self.image_instance.create_image(image_name)

    def delete_image(self, img_id=None):
        """Deregister image."""
        self.terminate_instances()
        return self.ec2.deregister_image(
            self.img_id if not img_id else img_id
            )
