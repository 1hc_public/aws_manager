"""Module to make accessible an object to simplify distributed ec2 ops."""
# Copyright (c) 2019-2021 1 Howard Capital LLC
import time
from requests import get
from multiprocessing.dummy import Pool as ThreadPool

import boto
from boto.ec2 import connect_to_region as connect_to_ec2_region_boto
from pssh.clients import ParallelSSHClient

from aws_manager.core.base import AWS_Base


class EC2(AWS_Base):
    """High level object to simplify ec2 ops (particularly distributed)."""

    def __init__(self, region='us-east-1', security_groups=['master'],
                 pem_key=None, pem_path=None, pwd='debian1234', **kwargs):
        """
        Connect ec2 client.

        Parameters
        ----------
        region : Str, optional
            AWS Region. The default is 'us-east-1'.
        security_groups : [Str], optional
            List of AWS security groups.
        pem_key : Str, optional
            Name of key used to connect to instance(s).
        pem_path : Str, optional
            Path to key used to connect to instance(s).
        pwd : Str
            Password used to connect to instance(s).
            Default connection method.

        Returns
        -------
        None.

        """
        super().__init__(**kwargs)
        self.pem = pem_key
        self.pem_path = pem_path
        self.sg_name = [*security_groups]
        self.password = pwd
        self.ec2 = connect_to_ec2_region_boto(region)
        self.instances = []
        self.pclient = None
        self._sshd_ref_dict = {
            '#PermitRootLogin prohibit-password': 'PermitRootLogin yes',
            '#PubkeyAuthentication yes': 'PubkeyAuthentication no',
            'PasswordAuthentication no': 'PasswordAuthentication yes',
            'IgnoreUserKnownHosts no': 'IgnoreUserKnownHosts yes'
            }
        # Modify SSHD_Config to allow for password auth.
        self.information = '#!/bin/bash\n' + ''.join([
            f'echo -e "{self.password}\n{self.password}" ',
            '| sudo passwd ubuntu'
            ]) + '\nsudo apt-get install update\n' + \
            '\n'.join([
                f"sudo sed -i -e 's/{k}/{v}/g' /etc/ssh/sshd_config"
                for k, v in self._sshd_ref_dict.items()
            ]) + '\n' + 'sudo wget https://bootstrap.pypa.io/get-pip.py' \
            + '\n' + 'sudo python3 get-pip.py'

    def boot_instances(self, img,
                       instance_type='t3.micro',
                       instance_count=1,
                       **kwargs):
        """
        Launch instance_count number of img AMI running on instance_type.

        Parameters
        ----------
        img : Str
            String corresponding to an accessible AMI.
            Ex. 'ami-123hkj1l23h8563'
        instance_type : Str
            Type of instance. The default is 't3.nano'.
        instance_count : Int
            Number of instances to launch. The default is 1.
        **kwargs : Multiple
            Any additional kwargs to be supplied to ec2.run_instances().

        Returns
        -------
        [boto.ec2.instance.Instance]
            List of boto object(s) representing AWS instance(s).

        """
        # Handle sshd for testing purposes.
        if '\nsudo service sshd restart' not in self.information:
            self.information = self.information \
                + '\nsudo service sshd restart\nsudo reboot'

        new_instances = self.ec2.run_instances(
            img,
            key_name=self.pem,
            instance_type=instance_type,
            min_count=instance_count,
            max_count=instance_count,
            security_groups=self.sg_name,
            user_data=self.information,
            **kwargs
            ).instances

        self.instances.extend(new_instances)

        return new_instances

    def _boot_default(self):
        self.boot_instances('ami-03d315ad33b9d49c4')
        self.permission_security()

    def init_pclient(self):
        """
        Initialize parallel ssh client.

        Returns
        -------
        None.

        """
        # Get running instances
        running = [i for i in self.instances
                   if i.update() == 'running']

        self.pclient = ParallelSSHClient(
            [i.public_dns_name for i in running],
            user='ubuntu',
            password=self.password
            ) if not self.pem_path else \
            ParallelSSHClient(
            [i.public_dns_name for i in running],
            user='ubuntu',
            pkey=self.pem_path
            )

    def terminate_instances(self):
        """
        Terminate all instances associated with EC2 object.

        Returns
        -------
        None.

        """
        _counter = 0
        while _counter < 4:
            for instance in self.instances:
                instance.terminate()

            _counter += 1

        def _close(instance):
            while instance.update() != 'terminated':
                instance.terminate()
                time.sleep(15)

        with ThreadPool() as p:
            p.map(_close,
                  self.instances
                  )

    def update_status(self):
        """
        Check status of instances.

        Returns
        -------
        status : {instance: str}
            What state the instance(s) are currently in.
            For example: {i-...: 'running'}.

        """
        status = {
            instance: instance.update()
            for instance in self.instances
            }

        return status

    def permission_security(self):
        """
        Attempt to fetch ip and permission to security group.

        Returns
        -------
        None.

        """
        try:
            cidr = f"{get('https://api.ipify.org').text}/32"
            # TODO Use PathManager for security group name.
            gn = self.sg_name[0]
            self.ec2.authorize_security_group(group_name=gn,
                                              from_port=22,
                                              to_port=22,
                                              ip_protocol='tcp',
                                              cidr_ip=cidr)
        except boto.exception.EC2ResponseError:
            # Already authorized.
            pass
