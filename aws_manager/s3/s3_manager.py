"""Module to make accessible an object to simplify S3 operations."""
# Copyright (c) 2019-2021 1 Howard Capital LLC
from io import StringIO, BytesIO

import pandas as pd
import boto3

from aws_manager.core.base import AWS_Base


class S3(AWS_Base):
    """High level object to simplify s3 management."""

    def __init__(self, **kwargs):
        """Initialize s3 connection."""
        super().__init__(**kwargs)
        self.s3 = boto3.client(
            's3',
            aws_access_key_id=self.aws_access_key,
            aws_secret_access_key=self.aws_secret
            )

    def build_bucket(self, bucket_name, bucket_kwargs={}):
        """Create specified S3 bucket."""
        self.set_bucket(bucket_name)
        return self.s3.create_bucket(
            Bucket=bucket_name,
            **bucket_kwargs
            )

    def delete_bucket(self, bucket_name, bucket_kwargs={}):
        """Delete specified S3 bucket."""
        return self.s3.delete_bucket(
            Bucket=bucket_name,
            **bucket_kwargs
            )

    def set_bucket(self, bucket_name):
        """Set bucket for S3 object."""
        self.bucket = bucket_name

    def write_pandas(self, df, loc):
        """Write stream of pandas df to s3."""
        with StringIO() as buffer:
            df.to_csv(buffer)
            buffer_bytes = BytesIO(buffer.getvalue().encode())
            return self.s3.put_object(
                Body=buffer_bytes,
                Bucket=self.bucket,
                Key=loc
                )

    def read_pandas(self, loc):
        """Read stream of pandas df to s3."""
        obj = self.s3.get_object(
                    Bucket=self.bucket,
                    Key=loc
                    )
        return pd.read_csv(obj['Body'], index_col=0)

    def exists(self, path_file):
        """Check if file exists."""
        try:
            self.s3.get_object(
                Bucket=self.bucket,
                Key=path_file
                )
            return True
        except self.s3.exceptions.NoSuchKey:
            return False

    def last_mod(self, path_file):
        """Check and return time of last modification."""
        _r = self.s3.get_object(
            Bucket=self.bucket,
            Key=path_file
            )
        return _r['LastModified']

    def delete_file(self, path_file):
        """Delete file."""
        return self.s3.delete_object(
            Bucket=self.bucket,
            Key=path_file
            )
